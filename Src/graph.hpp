#ifndef _GRAPH
#define _GRAPH
#include "board.hpp"
using namespace RR;
#include <vector>
#include <unordered_map>
using namespace std;




class Graph{
public:	
	Graph(const Board g); // Creer le constructeur de Graph

	struct Voisins
	{
		int orientation; // hop l'orientation du robot de depart
		int deplacement; // MOOVE depl entre rbd et rba
		Robot robot; // le robot a qui on a appliquer le deplacemenet
	};

	struct sommet 
	{

		vector<Voisins> voisins; // on creer un tableau dyn des voisins (robots)

	};

void afficheGraph(Board g);


private:
unordered_map<Location, sommet, LocationHash> graphe; // stocke les aretes et robots du board


};

#endif


