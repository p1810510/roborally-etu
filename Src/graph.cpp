#include "graph.hpp"
using namespace std;
#include <iostream>
Graph::Graph( Board g) {
    for(pair<Location, Board::TileType> tile : g.tiles) { // on parcours toutes les cases du board 
        sommet s;
        for(int i=0; i<4; i++){ // Orientation possible

            for(int j=0; j<7; j++){ // 7 coups possibles
                Voisins v;
                v.orientation = i;
                v.deplacement = j;
                Robot r = {tile.first, (Robot::Status) i} ;
                g.play(r , (Robot::Move) j);
                v.robot = r;
                if(v.robot.status!=Robot::Status::DEAD){ //je ne stock plus les robot qui mamen a la mort
                    //parcourt liste voisins, si location identique, on stocke pas
                    bool alreadyInVoisins = false;
                    for (Voisins v2 : s.voisins) {
                        if (v2.robot.location == v.robot.location) {
                            alreadyInVoisins = true; // déjà stocké, on sort de la boucle
                            break;
                        }
                    }
                    if (!alreadyInVoisins) { // s'il n'est pas déjà stocké, alors on le rajoute
                        s.voisins.push_back(v);
                }
            }
        }
        graphe[tile.first] = s;

    }
}
}



void Graph::afficheGraph(Board g) 
{	
	for(pair<Location, Board::TileType> cases : g.tiles) {
		

		for (size_t i=0; i<graphe[cases.first].voisins.size(); i++){ // v est un voisins 
			std::cout << " Je vais de [" << cases.first.column << ","; // rbd
			std::cout << cases.first.line;
			std::cout << ","  <<graphe[cases.first].voisins[i].orientation<<"]";

			std::cout << " avec le coup " <<graphe[cases.first].voisins[i].deplacement<< " ";
			std::cout << " vers [" << graphe[cases.first].voisins[i].robot.location.column << ","; // rbd
			std::cout << graphe[cases.first].voisins[i].robot.location.line<< ",";
			std::cout << static_cast<int>(graphe[cases.first].voisins[i].robot.status)<< "]"<<std::endl;

			// je vais de 1,3,0 à 1,5,0 avec le coup 1 // 
	
	}
}

}

